﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class karakterkontrol : MonoBehaviour
{

    public float speed;
    public Vector3 userAcceleration;

    private Rigidbody rb;
    private int count;
    float moveHorizontal;
    float moveVertical;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        userAcceleration = new Vector3 (0, 0, 0);
        moveHorizontal = 0.0f;
       moveVertical = 0.0f;
    }

    void FixedUpdate()
    {
        if (SystemInfo.deviceType == DeviceType.Desktop)
        {
             moveHorizontal = Input.GetAxis("Horizontal");
            moveVertical = Input.GetAxis("Vertical");

            Vector3 movement = new Vector3(moveVertical, 0.0f, -moveHorizontal);

            rb.AddForce(movement * speed);
        }
        else
        {
            moveHorizontal = Input.acceleration.x;
            moveVertical = Input.acceleration.y;
            Vector3 movement = new Vector3(moveVertical, 0.0f, -moveHorizontal);
            rb.AddForce(movement * speed * 1);
        }
    }

    
}