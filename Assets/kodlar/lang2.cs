﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lang2 : MonoBehaviour {

    public GameObject a, i, t;
    public int langint;

    void Start () {
       
		
	}
	
	// Update is called once per frame
	void Update () {
        langint = PlayerPrefs.GetInt("lang");
        if (langint == 1)
        {
            i.gameObject.SetActive(true);
            a.gameObject.SetActive(false);
            t.gameObject.SetActive(false);
        }
        if (langint == 2)
        {
            i.gameObject.SetActive(false);
            a.gameObject.SetActive(false);
            t.gameObject.SetActive(true);
        }
        if (langint == 3)
        {
            i.gameObject.SetActive(false);
            a.gameObject.SetActive(true);
            t.gameObject.SetActive(false);
        }

    }
}
